package model;

import java.util.List;

import java.util.Vector;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Group {
	private String nom;
	private ObservableList<Contact> list_contact;

	@JsonIgnore
	public ObservableList<Contact> getList_contact() {
		return list_contact;
	}
	
	@JsonIgnore
	public void setList_contact(ObservableList<Contact> list_contact) {
		if(this.list_contact == null) this.list_contact.clear();
		this.list_contact = list_contact;
		
	}
	
	@JsonGetter("nom")
	public String getNom() {
		return nom;
	}
	
	@JsonSetter("nom")
	public void setNom(String nom) {
		this.nom = nom;
	}

	public Group() {
		this.nom = "Default name";
		this.list_contact = FXCollections.observableArrayList(new Vector<>());
	}
	@JsonGetter("listContact")
	public List<Contact> getListContact() { 
		return list_contact.stream().collect(Collectors.toList());
	}

	
	@JsonSetter("listContact")
	public void setListContact(List<Contact> listContact) {
		if(listContact == null) list_contact.clear();
	    else {
	    	
		    listContact.forEach(contact -> {	
		        list_contact.add(contact);
		    	
		    });
	    }
	}

	@Override
	public String toString() {
	    return this.nom;
	}


}
