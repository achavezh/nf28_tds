package model;

import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;


public class Groups {
	private ObservableList<Group> groups = FXCollections.observableArrayList(new Vector<>());
	
    
	public ObservableList<Group> getGroups() {
		return groups;
	}

    public List<Group> getListGroup(){ 
    	return groups.stream().collect(Collectors.toList()); 
    }

    public void setListGroup (List<Group> listGroup){
    	groups.clear();
        listGroup.forEach(group -> {
        	groups.add(group);
            List<Contact> listContact =  group.getListContact();
            group.setListContact(listContact);
            
        });
    }

}