package model;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import com.fasterxml.jackson.annotation.*;

public class Contact{
	private StringProperty firstName;
	private StringProperty lastName;
	private StringProperty country;
	private StringProperty city;
	private StringProperty gender;
	private BooleanProperty valide;
	
	@Override
	public String toString() {
	    return getFirstNameString() + " " + getLastNameString();
	}
	@JsonIgnore
	public StringProperty getFirstName() {
		return firstName;
	}
	@JsonIgnore
	public StringProperty getLastName() {
		return lastName;
	}
	@JsonIgnore
	public StringProperty getCountry() {
		return country;
	}
	@JsonIgnore
	public StringProperty getCity() {
		return city;
	}
	@JsonIgnore
	public StringProperty getGender() {
		return gender;
	}
	
	@JsonGetter("firstName")
	public String getFirstNameString() {
		return firstName.get();
	}
	
	@JsonGetter("lastName")
	public String getLastNameString() {
		return lastName.get();
	}
	
	@JsonGetter("country")
	public String getCountryString() {
		return country.get();
	}
	
	@JsonGetter("city")
	public String getCityString() {
		return city.get();
	}
	
	@JsonGetter("gender")
	public String getGenderString() {
		return gender.get();
	}
	
	@JsonGetter("valide")
	public Boolean getValideBoolean() {
		return valide.get();
	}
	
	@JsonIgnore
	public BooleanProperty getValide() {
		return valide;
	}
	
	@JsonSetter("firstName")
	public void setFirstName(String firstName) {
		this.firstName.set(firstName);
	}
	
	@JsonSetter("lastName")
	public void setLastName(String lastName) {
		this.lastName.set(lastName);
	}
	
	@JsonSetter("country")
	public void setCountry(String country) {
		this.country.set(country);
	}
	
	@JsonSetter("city")
	public void setCity(String city) {
		this.city.set(city);
	}
	
	@JsonSetter("gender")	
	public void setGender(String gender) {
		this.gender.set(gender);
	}
	
	@JsonSetter("valide")
	public void setValide(Boolean valide) {
		this.valide.set(valide);
	}
	
	
	public Contact() {
		this.firstName = new SimpleStringProperty();
		this.lastName = new SimpleStringProperty();
		this.country = new SimpleStringProperty();
		this.city = new SimpleStringProperty();
		this.gender = new SimpleStringProperty();
		this.valide = new SimpleBooleanProperty();
		this.valide.set(false);
	}
	public Contact(String ln, String fn, String co, String ci, String gen, Boolean val) {
		
		this.firstName = new SimpleStringProperty();
		this.lastName = new SimpleStringProperty();
		this.country = new SimpleStringProperty();
		this.city = new SimpleStringProperty();
		this.gender = new SimpleStringProperty();
		this.valide = new SimpleBooleanProperty();

		this.firstName.set(fn);
		this.lastName.set(ln);
		this.country.set(co);
		this.city.set(ci);
		this.gender.set(gen);
		this.valide.set(val);
		
		
	}

	
}
