package model;

import java.util.List;
import java.util.stream.Collectors;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;

public class GraphiqueTool {
	private Groups groupes;
	private ObservableList<PieChart.Data> groupPieChartData;
	ObjectProperty<XYChart.Series<String, Number>> contactCity;
	

	public GraphiqueTool(Groups g) {
		groupes = g;
		groupPieChartData = FXCollections.observableArrayList();
		contactCity = new SimpleObjectProperty<>();
	}
	public List<PieChart.Data> contactPieChartData(){
		return groupPieChartData;
		
	}
	public XYChart.Series<String, Number> citiesSeriesData(){
		return contactCity.get();
		
	}
	public Groups getGroupes() {
		return groupes;
	}
	public void setGroupes(Groups groupes) {
		this.groupes = groupes;
	}
	public ObservableList<PieChart.Data> getGroupPieChartData() {
		return groupPieChartData;
	}
	
	public void setGroupPieChartData(ObservableList<PieChart.Data> groupPieChartData) {
		this.groupPieChartData = groupPieChartData;
	}
	
	public ObjectProperty<XYChart.Series<String, Number>> getContactCity() {
		return contactCity;
	}
	
	public void setContactCity(ObjectProperty<XYChart.Series<String, Number>> contactCity) {
		this.contactCity = contactCity;
	}
	/** Load Data into chart**/

	public void chargeData() {
		/** GROUP **/

		List<PieChart.Data> data =
                groupes.getListGroup()
                .stream()
                .map(group ->
                        new PieChart.Data(
                                group.getNom(),
                                group.getListContact().size()
                        )
                ).collect(Collectors.toList());

		groupPieChartData.clear();
		groupPieChartData.addAll(data);
		
		/** CONTACTS BY CITY **/

		XYChart.Series<String, Number> citySeries = new XYChart.Series<>();
		
        groupes.getListGroup().stream()
                .flatMap(group -> group.getListContact().stream())
                .collect(
                        Collectors.groupingBy(
                                contact -> contact.getCityString(),
                                Collectors.counting())
                )
                .forEach((city, nb) -> citySeries.getData().add(
                                new XYChart.Data<>(city, nb)));

        getContactCity().setValue(citySeries);

		
	}
}
