package model;


public class Error {
	private String title;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	
	public Error(String t, String m) {
		this.title = t;
		this.message = m;
	}
	
	
}
