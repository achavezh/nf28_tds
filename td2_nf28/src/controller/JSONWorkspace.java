package controller;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import model.Group;
import model.Groups;
import model.Error;

public class JSONWorkspace {
	
	private File file;
	Groups grp;
	private ObjectProperty<Error> error;

	public JSONWorkspace(Groups g) {
		this.file = null;
		this.grp = g;
		this.error = new SimpleObjectProperty<>();
	}
	
	public ObjectProperty<Error> getError() {
		return error;
	}
	
	public void setError(Error error) {
		this.error.setValue(error);;
	}
	
	public File getFile() { 
		return this.file; 
	}
	
	public void setGroup(Groups g) { 
    	this.grp = g; 
    }
	
	public void setFile(File file) {
		this.file = file; 	
    }
	
	public boolean hasFile() {
		if (this.file == null) return false;
		else return true;
	}
	public ArrayList<Group> fromFile(File file) throws Exception{
		setFile(file);
		ObjectMapper mapper = new ObjectMapper();
	    mapper.getFactory().configure(JsonGenerator.Feature.ESCAPE_NON_ASCII, true);
		ArrayList<Group> fv = null;

		fv = mapper.readValue(getFile(), new TypeReference<List<Group>>() {});
		
		return fv;
		
	}

	public void save(File f) throws IOException{
		FileWriter fw = new FileWriter(f);
		ObjectMapper mapper = new ObjectMapper();
		//ALLOW latin-1 CHARACTERS
		mapper.getFactory().configure(JsonGenerator.Feature.ESCAPE_NON_ASCII, true);
		String s = "";
		try {
			s = mapper.writeValueAsString(grp.getListGroup());
		} catch (IOException e) {
			e.printStackTrace();
			setError(new Error("Enregistrer le fichier", "Erreur pour enregistrer le fichier"));
		}
		
		fw.write(s);
		fw.close();

	}


}
