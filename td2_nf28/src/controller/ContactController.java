package controller;
import model.Contact;
import model.Country;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;


public class ContactController implements Initializable{
	
	private Contact person = new Contact();
	
    public Contact getPerson() {
		return person;
	}
    public void setPerson(Contact p) {
		person = p;
	}
    

	@FXML
    private RadioButton choice_fem;

    @FXML
    private RadioButton choice_mas;

    @FXML
    private Button validate_;

    @FXML
    private ChoiceBox<String> country;

    @FXML
    private TextField city;

    @FXML
    private TextField input_firstName;

    @FXML
    private TextField input_lastName;
    
    
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		/** Initialize ChoiceBox **/
		country.getItems().setAll(Country.getCountryNames());
	
		/** Binding **/
		input_firstName.textProperty().bindBidirectional(person.getFirstName());
		
		input_lastName.textProperty().bindBidirectional(person.getLastName());

		city.textProperty().bindBidirectional(person.getCity());
		
		country.getSelectionModel().selectedItemProperty().addListener(e -> {
			person.getCountry().set(country.getSelectionModel().getSelectedItem());
		});
		
		if(choice_fem.isSelected()) {
			person.getGender().set("Fem.");
		}
		
		choice_fem.selectedProperty().addListener( e -> {
			person.getGender().set("Fem.");
		});
		
		choice_mas.selectedProperty().addListener( e -> {
			person.getGender().set("Masc.");
		});				
	}
	private void styleCheckedProperty() {
		/** Style champs **/
		if(person.getFirstNameString() == null || input_firstName.getText().isEmpty()) {
			input_firstName.setStyle(("-fx-border-color: red ;"));
			input_firstName.setTooltip(new Tooltip("Le pr�nom doit �tre renseign�")); 
		}else {
			input_firstName.setStyle(("-fx-border-color: blue ;"));
		}
		
		if(person.getLastNameString() == null  || input_lastName.getText().isEmpty()) {
			input_lastName.setStyle(("-fx-border-color: red ;"));
			input_lastName.setTooltip(new Tooltip("Le nom doit �tre renseign�"));
		}else {
			input_lastName.setStyle(("-fx-border-color: blue ;"));
		}
		
		if(person.getCityString() == null || city.getText().isEmpty()) {
			city.setStyle(("-fx-border-color: red ;"));
			city.setTooltip(new Tooltip("La ville doit �tre renseign�e"));
		}else {
			city.setStyle(("-fx-border-color: blue ;"));
		}
		
		if(person.getCountryString() == null) {
			country.setStyle(("-fx-border-color: red ;"));
			country.setTooltip(new Tooltip("Le pays doit �tre renseign�"));
		}else {
			country.setStyle(("-fx-border-color: blue ;"));
		}
	}
	@FXML
	private void handleBtnValidate(ActionEvent event) {
		/** Dispatched action on clicked button validate **/

		styleCheckedProperty();
		
		if(person.getCountryString() == null || person.getCityString() == null ||
		person.getLastNameString() == null || person.getFirstNameString() == null || city.getText().isEmpty() ||
		input_lastName.getText().isEmpty() || input_firstName.getText().isEmpty()) {
			person.getValide().set(false);
		}else {
			person.getValide().set(true);
		}

	}
	public void resetPaneStyle() {

		city.setStyle(("-fx-border-color: transparent;"));
		city.setTooltip(null);
		
		input_firstName.setStyle(("-fx-border-color: transparent;"));
		input_firstName.setTooltip(null);
	
		input_lastName.setStyle(("-fx-border-color: transparent;"));
		input_lastName.setTooltip(null);
		
		country.setStyle(("-fx-border-color: transparent;"));
		country.setTooltip(null);

	}
	public void resetContact() {
		resetPaneStyle();
		person.setCity(new String(""));
		person.setFirstName(new String(""));
		person.setLastName(new String(""));
		choice_fem.setSelected(true);
		person.setCountry(new String(""));
		country.setValue(null);
		
		
	}
	public void loadContact(Contact c) {
		resetPaneStyle();
		
		person.setCity(new String(c.getCityString()));
		person.setFirstName(new String(c.getFirstNameString()));
		person.setLastName(new String(c.getLastNameString()));
		person.setCountry(new String(c.getCountryString()));
		person.setGender(new String(c.getGenderString()));
		
		if(c.getGenderString().equals("Fem.")) {
			choice_fem.setSelected(true);
		}else if(c.getGenderString().equals("Masc.")){
			choice_mas.setSelected(true);
		}
		
		country.setValue(person.getCountryString());
		
	}
	
	
}
