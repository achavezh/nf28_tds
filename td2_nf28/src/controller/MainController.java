package controller;

import java.io.File;
import java.io.IOException;

import javafx.scene.control.Menu;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.collections.ListChangeListener;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import model.Contact;
import model.Group;
import model.Groups;
import model.GraphiqueTool;
import model.Error;


public class MainController implements Initializable{
	
	ContactController contactController = new ContactController();
	Parent contactPane;
	Groups groupController = new Groups();
	JSONWorkspace workspace = new JSONWorkspace(groupController);
	GraphiqueTool graphiqueT = new GraphiqueTool(groupController);
	
	private Image groupIcon = new
			Image(getClass().getResourceAsStream("group.png"));
	private Image personIcon = new
			Image(getClass().getResourceAsStream("person.png"));
	
	@FXML
	private MenuBar menuBar;
	
	@FXML
	private MenuItem menuItemOuvrir;
	
	@FXML
	private MenuItem menuItemSave;
	
    @FXML
    private Menu Menu;
    
	@FXML
    private BorderPane rootPane = new BorderPane();
	
	@FXML
    private TreeView<Object> treeView;

	@FXML
	private Button addBtn;

    @FXML
    private Button deleteBtn;

    @FXML
    private Tab graphique;

    @FXML
    private Tab editionPane;

    @FXML
    private AnchorPane editingPane = new AnchorPane();
    
    @FXML
    private BarChart<String, Number> barChartCity;
    
    @FXML
    private PieChart pieChartGroupes;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		ContactControl cc = new ContactControl();
		contactController = cc.contactController;
		contactPane = cc.root;
		
		editionPane.setContent(contactPane);
		contactPane.visibleProperty().set(false);
		
		/***************LISTENER LISTE DE CONTACTS *****************/
		ListChangeListener<Contact> contactListener = p->{
			p.next();
			if(p.wasAdded()) {
				p.getAddedSubList().forEach(c -> {
					if (c != null) {
						ImageView image = new ImageView(personIcon);
						image.setFitHeight(18);
						image.setFitWidth(18);
						
						TreeItem<Object> newItem = new TreeItem<Object>(c, image);
						
                        treeView.getSelectionModel().selectedItemProperty().getValue().getChildren().add(newItem);
					 	newItem.setExpanded(true);
					 	treeView.getSelectionModel().select(newItem.getParent());
	                }
				});
			}
		
		};
		/***************LISTENER LISTE DE GROUPES*****************/

		groupController.getGroups().addListener((ListChangeListener<? super Group>) p ->{
			p.next();
			if(p.wasAdded()) {
				p.getAddedSubList().forEach(c -> {
                    if (c != null) {
                    	contactPane.visibleProperty().set(false);
						ImageView image = new ImageView(groupIcon);
						image.setFitHeight(20);
						image.setFitWidth(20);
						
						TreeItem<Object> newItem = new TreeItem<Object>(c, image);
						
                    	treeView.getRoot().getChildren().add(newItem);
                    	
						newItem.setExpanded(true);
						treeView.setEditable(true);
						treeView.setCellFactory(param -> new application.TextFieldTreeCell());
						
							c.getList_contact().addListener(contactListener);
						
						
						treeView.getSelectionModel().select(newItem);
                    }
				});
			}
			
			
		});
		/***************LISTENER TREEVIEW SELECTED ITEM *****************/

		treeView.getSelectionModel().selectedItemProperty().addListener( e -> {
			contactPane.visibleProperty().set(false);

			addBtn.setOnAction(evt -> {
							
				TreeItem<Object> treeItemSelected = new TreeItem<Object>();
				treeItemSelected = treeView.getSelectionModel().getSelectedItem(); //Get item Selected
				
				if(treeView.getTreeItemLevel(treeItemSelected) == 0) {//get Node level; level 0 => Root
					Group g = new Group();
					groupController.getGroups().add(g);		
					
				}

				if(treeView.getTreeItemLevel(treeItemSelected) == 1) {//level 1 => Group
					contactController.resetPaneStyle();
					contactController.resetContact();
					contactPane.visibleProperty().set(true);

				}
				
			});
			
			/***************LOAD CONTACT IN FORM ContactController *****************/

			Object selectedItem = treeView.getSelectionModel().getSelectedItem().getValue();
				
			if(selectedItem instanceof Contact){
				contactPane.visibleProperty().set(true);
				Contact cnt = (Contact) selectedItem;
				contactController.loadContact(cnt);
			}
			
		});
		/***************LISTENER ADD OR MODIFY CONTACT *****************/

		contactController.getPerson().getValide().addListener(p -> {
			if(contactController.getPerson().getValideBoolean() ) {
				addContact();
				contactController.getPerson().setValide(false);
			}else {
				Object selectedItem = treeView.getSelectionModel().getSelectedItem().getValue();
				
				if(selectedItem instanceof Contact){
					/******** MODIFICATION ********/
					Contact c = (Contact) treeView.getSelectionModel().getSelectedItem().getValue();
					c.setFirstName(contactController.getPerson().getFirstNameString());
					c.setLastName(contactController.getPerson().getLastNameString());
					c.setCity(contactController.getPerson().getCityString());
					c.setGender(contactController.getPerson().getGenderString());
					c.setCountry(contactController.getPerson().getCountryString());

				 	treeView.refresh();
				 	
				}
				
			}
		
		});

		/***************LISTENER CHART GROUP *****************/

	    graphiqueT.getGroupPieChartData().addListener((ListChangeListener<PieChart.Data>) p ->{
	    	p.next();
	    	pieChartGroupes.setData(graphiqueT.getGroupPieChartData());
	    });
	    
		/***************LISTENER CHART CONTACT BY CITY *****************/

	    graphiqueT.getContactCity().addListener(
	    		(v, oldValue, newValue) -> {
	    			barChartCity.getData().clear();
	    			barChartCity.getData().add( newValue);
	    	
	    });
		/***************LISTENER ERROR MESSAGE *****************/
	    Alert alert = new Alert(AlertType.ERROR);
        workspace.getError().addListener(
                (v, oldValue, newValue) -> {
                    alert.setTitle(newValue.getTitle());
                    alert.setContentText(newValue.getMessage());
                    alert.showAndWait();
                    //workspace.setError(new Error("",""));
                }
        );


	}
	/***************ON ADD ACTION ADD root*****************/

	@FXML
	private void handleBtnAdd(ActionEvent event) {
		
		TreeItem<Object> root = new TreeItem<>("Fiche Contacts");
		treeView.setRoot(root);
		root.setExpanded(true);
		
	}
	/*************** ON DELETE ACTION DELETE GROUP OR CONTACT *****************/

	@FXML
	private void handleBtnDelete(ActionEvent event) {
		
		
		Object selectedItem = treeView.getSelectionModel().getSelectedItem().getValue();
				
		if(selectedItem instanceof Group){
			Group grp = (Group) selectedItem;
			groupController.getGroups().removeAll(grp);
			treeView.getRoot().getChildren().remove(treeView.getSelectionModel().getSelectedItem());

		}
		if (treeView.getTreeItemLevel(treeView.getSelectionModel().getSelectedItem()) == 2) {//level 2 => Contact
			    	 
			Object parent = treeView.getSelectionModel().getSelectedItem().getParent().getValue();
		   	Contact cnt = (Contact) selectedItem;
	    	
			if(parent instanceof Group){
				Group grp = (Group) parent;
				
				grp.getList_contact().removeAll(cnt); 
				
				treeView.getSelectionModel().getSelectedItem().getParent().getChildren().remove(
        				treeView.getSelectionModel().getSelectedItem()
        		);
			}
			

		}
	}
	/*************** OPEN FILE *****************/

	@FXML
    private void menuItemClickedOpen(ActionEvent event) throws Exception {
        File userDirectory = new File(System.getProperty("user.dir") + "\\src\\json");
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(userDirectory);
        File file = fileChooser.showOpenDialog(null);
        try {
        	if (file != null) {
            	if(treeView.getRoot() == null) {
            		TreeItem<Object> root = new TreeItem<>("Fiche Contacts");
            		treeView.setRoot(root);
            		root.setExpanded(true);
            	}
            	groupController.setListGroup(workspace.fromFile(file));
            	
            }
            graphiqueT.chargeData();
        }catch(Exception e) {
        	workspace.setError(new Error("Ouverture du fichier", "Erreur pour ouvrir le fichier"));
        }
    }
	
	/*************** SAVE FILE *****************/

	@FXML
    private void menuItemClickedSave(ActionEvent event) throws IOException {
		
        File userDirectory = new File(System.getProperty("user.dir") + "\\src\\json");
        FileChooser fileChooser = new FileChooser();
        try {
	        if (workspace.hasFile()) {
	        	workspace.setFile(workspace.getFile());
	        	workspace.save(workspace.getFile());
	        }else {
	        	
	        	FileChooser.ExtensionFilter ext = new FileChooser.ExtensionFilter("Fichier", "*.json");
	            fileChooser.getExtensionFilters().add(ext);
	            fileChooser.setInitialDirectory(userDirectory);
	            File file = fileChooser.showSaveDialog(null);
	            workspace.save(file);
	        }
        }catch(Exception e) {
        	workspace.setError(new Error("Enregistrer le fichier", "Erreur pour enregistrer le fichier"));
        }
        
    }
	/*************** SAVE AS FILE *****************/

	@FXML
    private void menuItemClickedSaveAs(ActionEvent event) throws IOException {
		
        File userDirectory = new File(System.getProperty("user.dir") + "\\src\\json");
        FileChooser fileChooser = new FileChooser();
        //SET EXTENSION FILTER
        FileChooser.ExtensionFilter ext = new FileChooser.ExtensionFilter("Fichier", "*.json");
        fileChooser.getExtensionFilters().add(ext);
        fileChooser.setInitialDirectory(userDirectory);
        File file = fileChooser.showSaveDialog(null);
        try {
	        if (!workspace.hasFile()) {
	        	workspace.setFile(file);
	            
	            workspace.save(file);
	        }
        }catch(Exception e) {
        	workspace.setError(new Error("Enregistrer le fichier", "Erreur pour enregistrer le fichier"));
        }
        
    }
	/*************** REFRESH DATA INTO CHARTS *****************/

	@FXML
    private void rechargerData(ActionEvent event) throws IOException {
        graphiqueT.chargeData();
    }
	
	/*************** ADD CONTACT TO THE LIST => AFTER THAT THE LISTENER WILL BE DISPATCHED *****************/

	public void addContact() {
		
		Object selectedItem = treeView.getSelectionModel().getSelectedItem().getValue();
		if(selectedItem instanceof Group){
			Contact c = contactController.getPerson();
			Group grp = (Group) selectedItem;
        		
			//Copie du contact du contactController
	    	Contact contact_aux = new Contact(c.getLastNameString(), c.getFirstNameString(),
	 			c.getCountryString(), c.getCityString(), c.getGenderString(), c.getValideBoolean());
	 		
	 		grp.getList_contact().add(contact_aux);
			
		 }
		
	}
	
}
